package de.eis.bookr.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import de.eis.bookr.R;
import de.eis.bookr.model.Appointment;
import de.eis.bookr.other.RecyclerItemClickListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AppointmentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AppointmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AppointmentFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private DatabaseReference mTermine;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AppointmentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppointmentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AppointmentFragment newInstance(String param1, String param2) {
        AppointmentFragment fragment = new AppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mTermine = mDatabase.getReference("termine/"+mFirebaseUser.getUid());

       /* HashMap<String, String> h = new HashMap<String, String>() {{
            put("id","17895719571");
            put("name","Friseur");
        }};
        Appointment appointment = new Appointment("1473674400", "1473674400", h);
        mTermine.push().setValue(appointment);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            final FirebaseRecyclerAdapter<Appointment, AppointmentHolder>  mAdapter = new FirebaseRecyclerAdapter<Appointment, AppointmentHolder>(Appointment.class, R.layout.fragment_appointment_card, AppointmentHolder.class, mTermine) {
                @Override
                public void populateViewHolder(AppointmentHolder appointmentViewHolder, Appointment appointment, int position) {
                    appointmentViewHolder.setTime(getDate( Long.parseLong(appointment.getStart())));
                    appointmentViewHolder.setDienstleister(appointment.getDienstleister().get("name"));

                    /*recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            mAdapter.getRef(position).removeValue();
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                            mAdapter.getRef(position).removeValue();
                        }
                    }));*/



                }


            };

            recyclerView.setAdapter(mAdapter);
        }
        return view;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.GERMAN);
        cal.setTimeInMillis(time*1000);
        String date = DateFormat.format("dd. MMMM yyyy HH:mm", cal).toString();
        return date;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static class AppointmentHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        private final TextView mTimeField;
        private final TextView mDienstleisterField;

        /*public AppointmentHolderClickListener mListener;

        public static interface AppointmentHolderClickListener{
            public void onViewClick(View view, int position);
        }
*/
        public AppointmentHolder(View itemView) {
            super(itemView);
            mTimeField = (TextView) itemView.findViewById(R.id.time);
            mDienstleisterField = (TextView) itemView.findViewById(R.id.dienstleister);
            //mDienstleisterField.setOnClickListener(this);
            //itemView.setOnClickListener(this);
        }

        /*public void setCustomOnClickListener(AppointmentHolderClickListener listener  ){
            this.mListener = listener;
        }

        @Override
        public void onClick(View view) {


            if( mListener!= null ){
                        mListener.onViewClick(view, getAdapterPosition());
            }

        }*/

        public void setTime(String time) {
            mTimeField.setText(time);
        }

        public void setDienstleister(String dienstleister) {
            mDienstleisterField.setText(dienstleister);
        }
    }
}
