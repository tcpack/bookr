package de.eis.bookr.model;

/**
 * Created by fd on 10.12.2016.
 */

public class Category {
    public String name;
    public String surname;
    public String email;
    public static final String NAME_PREFIX = "Name_";
    public static final String SURNAME_PREFIX = "Surname_";
    public static final String EMAIL_PREFIX = "email_";
}
