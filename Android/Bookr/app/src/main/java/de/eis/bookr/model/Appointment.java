package de.eis.bookr.model;

import java.util.HashMap;

/**
 * Created by fd on 11.12.2016.
 */

public class Appointment {
    private String start;
    private String ende;
    private HashMap<String,String> dienstleister;


    public Appointment(){}

    public Appointment(String start, String ende, HashMap<String,String>  dienstleister){
        this.start = start;
        this.ende = ende;
        this.dienstleister = dienstleister;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnde() {
        return ende;
    }

    public void setEnde(String ende) {
        this.ende = ende;
    }

    public HashMap<String,String>  getDienstleister() {
        return dienstleister;
    }

    public void setDienstleister(HashMap<String,String>  dienstleister) {
        this.dienstleister = dienstleister;
    }


}
