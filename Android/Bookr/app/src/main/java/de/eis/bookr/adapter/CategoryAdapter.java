package de.eis.bookr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.eis.bookr.R;
import de.eis.bookr.fragment.SearchFragment;
import de.eis.bookr.model.Category;

/**
 * Created by fd on 10.12.2016.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

private List<Category> categoryList;
SearchFragment.OnFragmentInteractionListener mlistener;
Context mContext;

public CategoryAdapter(Context context, SearchFragment.OnFragmentInteractionListener mlistener, List<Category> contactList) {
        this.categoryList = contactList;
        this.mlistener = mlistener;
        this.mContext = context;
        }

@Override
public int getItemCount() {
        return categoryList.size();
        }

@Override
public void onBindViewHolder(CategoryViewHolder categoryViewHolder, int i) {
        final Category c = categoryList.get(i);
        categoryViewHolder.vName.setText(c.name);
        categoryViewHolder.vSurname.setText(c.surname);
        categoryViewHolder.vEmail.setText(c.email);
        categoryViewHolder.vTitle.setText(c.name + " " + c.surname);
        categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlistener.onCategorySelected(c);
            }
        });
        }

@Override
public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View itemView = LayoutInflater.
        from(parent.getContext()).
        inflate(R.layout.card_category, parent, false);

        return new CategoryViewHolder(itemView);
        }

public static class CategoryViewHolder extends RecyclerView.ViewHolder {
    protected TextView vName;
    protected TextView vSurname;
    protected TextView vEmail;
    protected TextView vTitle;

    public CategoryViewHolder(View v) {
        super(v);
        vName =  (TextView) v.findViewById(R.id.txtName);
        vSurname = (TextView)  v.findViewById(R.id.txtSurname);
        vEmail = (TextView)  v.findViewById(R.id.txtEmail);
        vTitle = (TextView) v.findViewById(R.id.title);
    }}
}
