/**
 * Global Require
 **/
queue = require('firebase-queue');
firebase = require('firebase');
fbadmin = require("firebase-admin");
moment = require('moment');
async = require('async');
/**
 * Local Require
 **/
var booking = require('./lib/bookingFuncs.js');
var Planing = require('./lib/planingFuncs.js');
var notifications = require('./lib/sendNotifications.js');
var OnEvents = require('./lib/fireBaseOnEvents.js')
//Set Moment to de
//0=Mo 1=Di 2=Mi 3=Do 4=Fr Sa=5 So=6
moment.locale("de");
/**
 * Init Firebase
 **/
fbadminapp = fbadmin.initializeApp({
    credential: fbadmin.credential.cert("./config/bookr-b43f5-firebase-adminsdk-jjnrg-54347101ed.json")
    , databaseURL: "https://bookr-b43f5.firebaseio.com"
});
fbapp = firebase.initializeApp({
    serviceAccount: './config/bookr-b43f5-firebase-adminsdk-jjnrg-54347101ed.json'
    , databaseURL: 'https://bookr-b43f5.firebaseio.com/'
});

auth = fbadminapp.auth();

/**
 * Local vars
 **/
firebasedb = firebase.database();
var refmyQueue = firebasedb.ref('my-queue');
//firebase.database.enableLogging(true, true);

var onEvents = new OnEvents(firebasedb);
var planing = new Planing(firebasedb,moment);
var options = {
    'specId': 'terminRequest'
    , 'numWorkers': 1
    , 'sanitize': true
    , 'suppressStack': true
};
var terminRequestQueue = new queue(refmyQueue, options, function (data, progress, resolve, reject) {
    booking.bookRequest(data.data, data.reply_to, function (err, res) {
        if (err) {
            reject(err);
            notifications.sendErrorToUser(err, data.reply_to, function (err, res) {
                if (err) console.log(err);
            });
        }
        else {
            resolve(data._id);
            notifications.sendNotificationToUser("Bookr Buchung", "Ihr Termin wurde erfolgreich gebucht.","bookrBookRequest", data.reply_to, function (err, res) {
                if (err) console.log(err);
            });
        }
    });
});
var options = {
    'specId': 'planRequest'
    , 'numWorkers': 1
    , 'sanitize': true
};
var planRequestQueue = new queue(refmyQueue, options, function (data, progress, resolve, reject) {
    progress(10);
    planing.planRequest(data.data, data.reply_to, data.data.duration, function (err, res) {
        if (err) {
            reject(err);
            notifications.sendErrorToUser(err, data.reply_to, function (err, res) {
                if (err) console.log(err);
            });
        }
        else {
            resolve(data._id);
            notifications.sendNotificationToUser("Termin Gebucht!", "Ihr Termin wurde erfolgreich gebucht.","bookrPlanRequest", data.reply_to, function (err, res) {
                if (err) console.log(err);
            });
        }
    });
});
var options = {
    'specId': 'gTokenRequest'
    , 'numWorkers': 1
    , 'sanitize': true
    , 'suppressStack': true
};
var gTokenRequestQueue = new queue(refmyQueue, options, function (data, progress, resolve, reject) {
    booking.registerToken(data.data.token, data.reply_to, function (err, res) {
        if (err) {
            stopProcessing();
            reject(err);
            notifications.sendErrorToUser(JSON.stringify(err), data.reply_to, function (err, res) {
                if (err) console.log(err);
            });
        }
        else {
            // refmyQueue.child(data._id).remove();
            resolve(data._id);
        }
    });
});

onEvents.on('appointmentRemoved', function (t, u) {
    notifications.sendNotificationToUser("Termin abgesagt!",t.dienstleister.name + ' um: ' + moment.unix(t.start).format() + ' wurde abgesagt.', "bookrRemoved", u, function (err, res) {
        if (err) console.log(err);
    });
});

onEvents.on('appointmentAdded', function (t, u) {
    notifications.sendNotificationToUser("Termin angelegt!",t.dienstleister.name + ' um: ' + moment.unix(t.start).format() + ' wurde abgesagt.', "bookrRemoved", u, function (err, res) {
        if (err) console.log(err);
    });
});