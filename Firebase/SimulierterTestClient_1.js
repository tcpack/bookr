var firebase = require('firebase');
var Queue = require('firebase-queue');
// TODO(DEVELOPER): Change the two placeholders below.
// [START initialize]
// Initialize the app with a service account, granting admin privileges
firebase.initializeApp({
    serviceAccount: './config/bookr-b43f5-firebase-adminsdk-jjnrg-54347101ed.json'
    , databaseURL: 'https://bookr-b43f5.firebaseio.com/'
});
firebase.database.enableLogging(true);
var firebasedb = firebase.database();
var myQueueRef = firebasedb.ref("my-queue");
var whichPlan = 0;
if (whichPlan == 0) {
    doPlan({
        "_state": "start_planning"
        , "reply_to": "j2ka08C31eQbqnkFkYNQ3ARqxqM2"
        , "data": {
            "start": "1485381887"
            , "end": "1485641123"
            ,"duration":30
            , "dienstleister": "freddyid"
            , "leistungen": {
                "0": "Damenschnitt"
            }
        }
    });
}
else if (whichPlan == 1) {
    doPlan({
        "_state": "start_book_appointment"
        , "reply_to": "fabianid"
        , "data": {
            "start": "1484514060"
            , "end": "1484517660"
            , "dienstleister": "freddyid"
            , "leistungen": {
                "1": "-KaZvXRKT8TkTLN1zyFa"
            }
        }
    });
}
else if (whichPlan == 2) {
    doPlan({
        "_state": 'start_get_token'
        , "reply_to": "j2ka08C31eQbqnkFkYNQ3ARqxqM2"
        , "data": {
            "token": '4/_xribLs8gbGmCXXMb-IDMR5rft-jDnMsMYBBfHKNosc'
        }
    });
}


function doPlan(l) {
    myQueueRef.child('tasks').push(l);
};

function showResults(data) {
    console.log(data);
};