/**
 * Global Require
 **/
queue = require('firebase-queue');
firebase = require('firebase');
fbadmin = require("firebase-admin");
moment = require('moment');
async = require('async');

/**
 * Local Require
 **/
var booking = require('./lib/bookingFuncs.js');
var planing = require('./lib/planingFuncs.js');
var notifications = require('./lib/sendNotifications.js');
var onEvents = require('./lib/fireBaseOnEvents.js');

//Set Moment to de
//0=Mo 1=Di 2=Mi 3=Do 4=Fr Sa=5 So=6
moment.locale("de");

/**
 * Init Firebase
 **/
fbadmin.initializeApp({
    credential: fbadmin.credential.cert("./config/bookr-b43f5-firebase-adminsdk-jjnrg-54347101ed.json")
    , databaseURL: "https://bookr-b43f5.firebaseio.com"
});
firebase.initializeApp({
    serviceAccount: './config/bookr-b43f5-firebase-adminsdk-jjnrg-54347101ed.json'
    , databaseURL: 'https://bookr-b43f5.firebaseio.com/'
});

/**
 * Local vars
 **/
var firebasedb = firebase.database();
var refmyQueue = firebasedb.ref('my-queue');
//firebase.database.enableLogging(true, true);

//onEvents.startListening();

var queue = new queue(refmyQueue, {
    sanitize: false
}, function (data, progress, resolve, reject) {
    switch (data.typ) {
    case 'TerminRequest':
        booking.bookRequest(data.data, data.reply_to, function (err, res) {
            if (err) {
                reject(err);
                notifications.sendErrorToUser(err,data.reply_to,function(err,res){
                    if (err) console.log(err);
                });
            }
            else {
                resolve(data._id);
                notifications.sendNotificationToUser("Termin Gebucht!","Ihr Termin wurde erfolgreich gebucht.",data.reply_to,function(err,res){
                    if (err) console.log(err);
                });
            }
        });
        break;
    case 'PlanRequest':
        planing.planRequest(data.data, data.reply_to, function (err, res) {
            if (err) {
                reject(err);
                notifications.sendErrorToUser(err,data.reply_to,function(err,res){
                    if (err) console.log(err);
                });
            }
            else {
                resolve(data._id);
                notifications.sendNotificationToUser("Termin Gebucht!","Ihr Termin wurde erfolgreich gebucht.",data.reply_to,function(err,res){
                    if (err) console.log(err);
                });
            }
        });
        break;
    case 'gTokenRequest':
        booking.registerToken(data.data.token, data.reply_to, function (err, res) {
            if (err) {
                reject(err);
                notifications.sendErrorToUser(JSON.stringify(err),data.reply_to,function(err,res){
                    if (err) console.log(err);
                });
            }
            else {
               // refmyQueue.child(data._id).remove();
                resolve(data._id);
            }
        });
        break;
    }
});

process.on('SIGINT', function() {
  console.log('Starting queue shutdown');
  queue.shutdown().then(function() {
    console.log('Finished queue shutdown');
    process.exit(0);
  });
});