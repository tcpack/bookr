var util = require('util');
var EventEmitter = require('events').EventEmitter;



var onEvents = function(firebasedb) {
    firebasedb = firebasedb;
    var self = this;

    var termineRef = firebasedb.ref('termine');
    termineRef.on('child_added', function (childsnapshot1, prevChildKey1) {
        if (childsnapshot1) {
            var userRef = firebasedb.ref('termine').child(childsnapshot1.key);
            userRef.on('child_added', function (childsnapshot2, prevChildKey2) {
                var tmpAppoint = childsnapshot2.val();
                if (tmpAppoint.dienstleister) {
                    var dlnAppointRef = firebasedb.ref('termine/' + tmpAppoint.dienstleister.id).child(tmpAppoint.id);
                    auth.getUser(childsnapshot1.key).then(function (user) {
                        var tmpObj = {
                            "dln": {
                                "id": childsnapshot1.key
                                , "name": user.providerDataInternal[0].displayNameInternal
                            }
                            , "end": tmpAppoint.end
                            , "id": tmpAppoint.id
                            , "start": tmpAppoint.start
                        }
                        dlnAppointRef.set(tmpObj);
                        self.emit('appointmentAdded', tmpObj, childsnapshot1.key);
                    });
                }
                else if (tmpAppoint.dln) {
                    var dlAppointRef = firebasedb.ref('termine/' + tmpAppoint.dln.id).child(tmpAppoint.id);
                    var dlnProfileRef = firebasedb.ref('profile/' + childsnapshot1.key);
                    dlnProfileRef.once('value').then(function (snapshot) {
                        var name = snapshot.val();
                        var tmpObj = {
                            "dienstleister": {
                                "id": childsnapshot1.key
                                , "name": name.name
                            }
                            , "end": tmpAppoint.end
                            , "id": tmpAppoint.id
                            , "start": tmpAppoint.start
                        }
                        dlAppointRef.set(tmpObj);
                    });
                }
                userRef.on('child_removed', function (childsnapshot5, prevChildKey5) {
                    var tmpAppoint = childsnapshot2.val();
                    var myKey = childsnapshot5.key;
                    if (tmpAppoint.dienstleister) {
                        console.log('tmpAppoint.dienstleister Ref:termine/' + tmpAppoint.dienstleister.id + '  child(' + tmpAppoint.id + ')');
                        var dlnAppointRef = firebasedb.ref('termine/' + tmpAppoint.dienstleister.id).child(myKey);
                        dlnAppointRef.remove();
                        self.emit('appointmentRemoved', tmpAppoint, childsnapshot1.key);
                    }
                    else if (tmpAppoint.dln) {
                        console.log('tmpAppoint.dln Ref:termine/' + tmpAppoint.dln.id + '  child(' + tmpAppoint.id + ')');
                        var dlAppointRef = firebasedb.ref('termine/' + tmpAppoint.dln.id).child(myKey);
                        dlAppointRef.remove();
                    }
                });
            });
        };
        /*
        termineRef.on('child_removed', function (childsnapshot6, prevChildKey6) {
            var userRef = firebasedb.ref('termine').child(prevChildKey6);
            userRef.off();
            console.log('child_removed');
        });
        */
    });
}
    


module.exports = onEvents;
util.inherits(onEvents, EventEmitter);