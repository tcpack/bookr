"use strict";
var gCal = require('../lib/gCalendarFuncs.js');
var gAuth = require('../lib/gAuthFunc.js');
var booking = require('../lib/bookingFuncs.js');
var IntervalTree = require('interval-tree2');



function PlaningFuncs(firebasedb,moment) {
    this.firebasedb = firebasedb;
    this.moment = moment;
}

module.exports = PlaningFuncs;

PlaningFuncs.prototype.planRequest = function (t, u, d, cb) {
        //convert the input span over the full time span
        var tmpStart = moment.unix(t.start);
        var tmpEnd = moment.unix(t.end);
        t.start = moment(moment("00:00:00", "hh:mm:ss").date(tmpStart.date()).month(tmpStart.month()).year(tmpStart.year())).unix();
        t.end = moment(moment("23:59:59", "hh:mm:ss").date(tmpEnd.date()).month(tmpEnd.month()).year(tmpEnd.year())).unix();
         this.getAllAppointments(t, u, function (err, res) {
            if (err) {
                cb(err);
            }
            else {
              PlaningFuncs.findGapsInAppointments(t, res, d, function (err, res) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        t.start = res[0].start;
                        t.end = res[0].end;
                        booking.bookService(t, u, function (err, res) {
                            if (err) {
                                cb(err);
                            }
                            else {
                                cb(null, res);
                            }
                        });
                    }
                });
            }
        });
    }







/**
 * Checks all appointments and returns the gaps with the write size. Depens on duration
 * @param {termin} t 
 * @param {json} o
 * @param {function} cb
 * @return {json} gaps
 * @throws No free spot.
 */
PlaningFuncs.findGapsInAppointments = function(t, o, d, cb) {
    try {
        var tDuration = (d * 60);
        var openDayArray = daysBetweenTwoMoments(moment.unix(t.start), moment.unix(t.end));
        var appointmentArray = [];
        var freeAppointments = [];
        var tmpObj = o.dlTermin;
        for (var i in tmpObj) {
            appointmentArray.push({
                "start": tmpObj[i].start
                , "end": tmpObj[i].end
            });
        }
        var tmpObj = o.dlnTermin;
        for (var i in tmpObj) {
            appointmentArray.push({
                "start": tmpObj[i].start
                , "end": tmpObj[i].end
            });
        }
        var tmpObj = o.gTermin;
        for (var i in tmpObj) {
            appointmentArray.push({
                "start": tmpObj[i].start
                , "end": tmpObj[i].end
            });
        }
       /*
        // remove duplicates from the array
        for (i = 0; i < appointmentArray.length - 1; i++) {
            if (appointmentArray[i].end == appointmentArray[i + 1].end && appointmentArray[i].start == appointmentArray[i + 1].start) { // abcdef -> if a + 1 = b then remove a
                appointmentArray.splice(i, 1);
            }
        }*/
        for (d in openDayArray) { //for every day in the timespan
            var tmpWeekDay = moment.unix(openDayArray[d]).weekday(); // get number of current weekday
            var tmpOpenTime = o.öffnungsZeiten[tmpWeekDay]; //get the times from object
            if (tmpOpenTime != undefined) {
                var startTime = moment(tmpOpenTime.startAm, "hh:mm"); //get startAm and conv. to moment
                var endTime = moment(tmpOpenTime.endPm, "hh:mm"); //get startAm and conv. to moment
                var tmpDate = moment.unix(openDayArray[d]); //get the current date
                var pauseStart = moment(tmpOpenTime.endAm, "hh:mm");
                var pauseEnd = moment(tmpOpenTime.startPm, "hh:mm");
                var tmpStartDateTime = moment(moment(startTime).date(tmpDate.date()).month(tmpDate.month()).year(tmpDate.year())).unix(); //new moment with date = current day and time = current day startAm
                var tmpEndDateTime = moment(moment(endTime).date(tmpDate.date()).month(tmpDate.month()).year(tmpDate.year())).unix(); //new moment with date = current day and time = current day endPm
                var tmpAppointmentArray = [];
                //if there is a pause for the day
                if (pauseStart.isValid() && pauseEnd.isValid()) {
                    var tmpPauseStartDateTime = moment(moment(pauseStart).date(tmpDate.date()).month(tmpDate.month()).year(tmpDate.year())).unix(); //new moment with date = current day and time = current day startAm
                    var tmpPauseEndDateTime = moment(moment(pauseEnd).date(tmpDate.date()).month(tmpDate.month()).year(tmpDate.year())).unix(); //new moment with date = current day and time = current day endPm
                    tmpAppointmentArray.push({
                        "start": tmpPauseStartDateTime
                        , "end": tmpPauseEndDateTime
                    });
                    tmpAppointmentArray.sort(compareAppointArray); //after removing sort array again
                }
                //make a tmp array for all apointments on the current day
                for (t in appointmentArray) {
                    if ((appointmentArray[t].end <= tmpEndDateTime && appointmentArray[t].end >= tmpStartDateTime)) {
                        tmpAppointmentArray.push(appointmentArray[t]);
                    }
                }
                appointmentArray.push({
                    "start": tmpEndDateTime
                    , "end": tmpEndDateTime + 69
                });
                tmpAppointmentArray.push({
                    "start": tmpStartDateTime - 69
                    , "end": tmpStartDateTime
                });
                tmpAppointmentArray.sort(compareAppointArray);
                appointmentArray.sort(compareAppointArray);
                var mittedesTages = tmpStartDateTime + (tmpEndDateTime - tmpStartDateTime) / 2;
                var dateTree = new IntervalTree(tmpStartDateTime);
                for (var l in appointmentArray) {
                    dateTree.add(appointmentArray[l].start, appointmentArray[l].end);
                }
                for (var k in tmpAppointmentArray) {
                    var allNull = true;
                    var treeSearch = dateTree.search(tmpAppointmentArray[k].end+1, tmpAppointmentArray[k].end + tDuration - 1);
                    for (var m in treeSearch) {
                        if (treeSearch[m].rate1 != 0) {
                            allNull = false;
                        }
                    }
                    if (treeSearch.length === 0 || allNull) {
                        freeAppointments.push({
                            "start": tmpAppointmentArray[k].end
                            , "end": tmpAppointmentArray[k].end + tDuration
                        });
                    }
                }
            }
        }
        if (freeAppointments.length > 0) {
            cb(null, freeAppointments);
        }
        else {
            cb("No free spot.");
        }
    }
    catch (err) {
        cb(err);
    }
}

/**
 * Compare a array of json with {start:time,end:time} to sort them
 * @param {json} a 
 * @param {json} b
 */
function compareAppointArray(a, b) {
    if (a.start < b.start) return -1;
    if (a.start > b.start) return 1;
    return 0;
}

/**
 * Gets all relevant appointment data
 * @param {termin} t 
 * @param {string} i
 * @param {function} cb
 * @return {json} json-obj with all the data in it
 * @throws Firebase Error
 */
PlaningFuncs.prototype.getAllAppointments = function (t, u, cb) {
    var openDayArray = daysBetweenTwoMoments(moment.unix(t.start), moment.unix(t.end));
    var dlTermineRef = firebase.database().ref('termine').child(t.dienstleister);
    var dlnTermineRef = firebase.database().ref('termine').child(u);
    var dlOeffnungsRef = firebase.database().ref('oeffnungszeiten/' + t.dienstleister);
    async.parallel({
        dlTermin: function (cb) {
            dlTermineRef.orderByChild("start").startAt(t.start).endAt(t.end).once('value').then(function (snapshot) {
                cb(null, snapshot.val());
            });
        }
        , dlnTermin: function (cb) {
            dlnTermineRef.orderByChild("start").startAt(t.start).endAt(t.end).once('value').then(function (snapshot) {
                cb(null, snapshot.val());
            });
        }
        , gTermin: function (cb) {
            gAuth.gAuth('', u, function (err, auth) {
                if (err) {
                    cb(err);
                }
                else {
                    gCal.eventsFrom(auth, t.start, t.end, function (err, res) {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null, res);
                        }
                    });
                }
            })
        }
        , öffnungsZeiten: function (cb) {
            dlOeffnungsRef.once('value').then(function (snapshot) {
                var tmpSnapshot = snapshot.val();
                var tmpObj = {};
                for (var i in openDayArray) {
                    tmpObj[moment.unix(openDayArray[i]).weekday()] = tmpSnapshot[moment.unix(openDayArray[i]).weekday()];
                }
                cb(null, tmpObj);
            });
        }
    }, function (err, res) {
        if (err) {
            cb(err);
        }
        else {
            cb(null, res);
        }
    });
}

/**
 * Gets all days between two moment objs with time = a.time
 * @param {moment} a 
 * @param {moment} b
 * @return {array} Unix Timestamps
 */
function daysBetweenTwoMoments(a, b) {
    var daysBetween = [];
    var diff = b.diff(a, 'days');
    var curentDay = a;
    if (diff == NaN || diff == 0) {
        var diff = 0;
    }
    for (var i = 0; i <= diff; i++) {
        daysBetween.push(curentDay.unix());
        curentDay = curentDay.add(24, 'hours');
    }
    return daysBetween;
}