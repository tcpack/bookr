var google = require('googleapis');
var exports = module.exports = {
    isBusy: isBusy,
    eventsFrom: function (auth, start,end, cb) {
        eventsFrom(auth, start,end, function (err, res) {
            if (err) {
                cb(err);
            }
            else {
                cb(null, res);
            }
        });
    }
};

function isBusy(auth, start, end, cb) {
    var calendar = google.calendar('v3');
    calendar.freebusy.query({
        auth: auth
        , headers: {
            "content-type": "application/json"
        }
        , resource: {
            timeMin: moment.unix(start).toISOString()
            , timeMax: moment.unix(end).toISOString()
            , timeZone: 'CET'
            , items: [{
                'id': 'primary'
            }]
        }
    }, function (err, res) {
        if (err) {
            cb(err);
        }
        else {
            var events = res.calendars['primary'].busy;
            if (events.length == 0) {
                cb(null, true);
            }
            else {
                cb(null, false);
            }
        }
    });
}

function eventsFrom(auth,start,end,cb) {
  var calendar = google.calendar('v3');
  calendar.events.list({
    auth: auth,
    calendarId: 'primary',
    timeMin: moment.unix(start).toISOString(),
    timeMax: moment.unix(end).toISOString(),
    maxResults: 100,
    singleEvents: true,
    orderBy: 'startTime'
  }, function(err, response) {
    if (err || response == null) {
       cb(err);
    }
    var events = response.items;
    if (events.length == 0) {
       cb(null,{});
    } else {
        var returnObj = {};
      for (var i = 0; i < events.length; i++) {
        var event = events[i];
        var start = event.start.dateTime || event.start.date;
        var end = event.end.dateTime || event.end.date;
        returnObj["termin"+i] = {"start":moment(start).unix(),"end":moment(end).unix()};
      }
        cb(null,returnObj);
    }
  });
}