FCM = require('fcm-node');
var fcm = new FCM('AAAAT5sB900:APA91bGziHPKjc2u0lJMX562qMEGO-1VgVfHN2EqtsP1yXg3ysKq0NVn7sr628Dv-90E7c7HqHbXdHHYa6lcXIMo6lyHKV469BKnwFI-BOpMd-gtsoC5LpfJJZC8i80FTsx9t03lVW2sQAUk9DnSwMU5omctwO92LQ');

var exports = module.exports = {
    sendNotificationToUser:sendNotificationToUser,
    sendErrorToUser:sendErrorToUser 
};
/**
 * Gets the user fcm token and send a notification
 * @param {string} titel
 * @param {string} body
 * @param {string} user
 * @param {function} cb
 * @throws Firebase Error
 */ 
function sendNotificationToUser(t, b,tag, u, cb) {
    getTokenByUser(u, function (err, res) {
        if (err) {
            cb(err);
        }
        else {
            var message = {
                priority: "high",
                to: res, // required fill with device token or topics
                notification: {
                    tag: tag,
                    title: t
                    , body: b
                }
            };
            fcm.send(message, function (err, response) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null,true);
                }
            });
        }
    });
}

/**
 * Send a error notification to the user
 * @param {json} myerr
 * @param {string} userID
 * @param {function} cb
 * @return {bool} true
 * @throws Firebase Error
 */ 
function sendErrorToUser(myerr,u,cb) {
      getTokenByUser(u, function (err, res) {
        if (err) {
            cb(err);
        }
        else {
            var message = {
                priority: "high",
                to: res,
                notification: {
                    tag: "BookrError",
                    title: "Fehler"
                    , body: myerr
                }
            };
            fcm.send(message, function (err, response) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null,true);
                }
            });
        }
    });  
}

/**
 * Get the fcm token by the userid
 * @param {string} userID
 * @param {function} cb
 * @return {string} token
 * @throws Firebase Error
 */ 
function getTokenByUser(u, cb) {    
     var dlnTokenRef = firebase.database().ref('user').child(u).child("fcmtoken");
        dlnTokenRef.once('value').then(function (snapshot) {
        var token = snapshot.val();
        if (token) {
            cb(null,token);
        } else {
            cb(null);
        } 
        });
}
