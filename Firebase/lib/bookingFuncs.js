var gCal = require('../lib/gCalendarFuncs.js');
var gAuth = require('../lib/gAuthFunc.js');

var exports = module.exports = {
    bookRequest: function (t, u, cb) {
        checkInterval(t, function (err,res) {
            if (err) {
                cb(err);
            }
            else {
                checkAvailability(t, u, function (err, res) {
                    if (err) {
                        cb(err);
                    }
                    else {
                         bookService(t, u, function (err, res) {
                            if (err) {
                              cb(err);
                            }
                          else {
                        cb(null, res);
                          }
                        });
                    }
                });
            }
        });
    }
    , registerToken: function (t, u, cb) {
        gAuth.gAuth(t, u, function (err, res) {
            if (err) {
                cb(err);
            }
            else {
                cb(null, res);
            }
        });
    },
    bookService : bookService
};


/**
 * Runs all functions needed to check availability
 * @param {termin} t 
 * @param {string} u
 * @param {function} cb
 */
function checkAvailability(t, u, cb) {
    async.parallel([
       function (callback) {
            checkÖffnung(t, function (err, res) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, res);
                }
            });
        }


        
        , function (callback) {
            checkAbwesenheit(t, function (err, res) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, res);
                }
            });
        }


        
        , function (callback) {
            checkTermine(t, t.dienstleister, function (err, res) { //termine des dienstleisters
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, res);
                }
            });
        }


        
        , function (callback) {
            checkTermine(t, u, function (err, res) { //termine selbst
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, res);
                }
            });
        }


        
        , function (callback) {
            checkServices(t, function (err, res) { //termine selbst
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, res);
                }
            });
        }


        
        , function (callback) {
            gAuth.gAuth('', u, function (err, auth) {
                if (err) {
                    callback(err);
                }
                else {
                    gCal.isBusy(auth, t.start, t.end, function (err, res) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            callback(null, res);
                        }
                    });
                }
            });
        }], function (err, res) {
        if (err) {
            cb(err);
        }
        else {
            cb(null, res);
        }
    });
}

/**
 * Checks if interval in appointment is correct
 * @param {termin} t 
 * @param {function} cb
 * @return {bool} true
 * @throws Wrong Interval
 */
function checkInterval(t, cb) {
    if (moment.unix(t.start).isValid() && moment.unix(t.end).isValid()) {
        if (moment.unix(t.start).isBefore(moment.unix(t.end))) {
            cb(null, true);
        }
        else {
            cb("Falsche Zeitangabe");
        }
    }
    else {
        cb("Falsche Zeitangabe");
    }
}

/**
 * Writes appointment for dl and dln in firebase
 * @param {termin} t 
 * @param {string} userID
 * @param {function} cb
 * @return {bool} true
 * @throws Firebase Error
 */
function bookService(t, userID, cb) {
    var userTermineRef = firebasedb.ref('termine').child(userID);
    var dlProfilRef = firebasedb.ref('profile').child(t.dienstleister);
    var leistungRef = firebasedb.ref('leistungen').child(t.dienstleister);
    leistungRef.once('value').then(function (snapshot) {
        leistungenByDl = snapshot.val();
        dlProfilRef.once('value').then(function (snapshot) {
            auth.getUser(userID).then(function (user) {
                var dlProfileData = snapshot.val();
                var newTerminKey = userTermineRef.push().key;
                var newT = {
                    'dienstleister': {
                        'id': t.dienstleister
                        , 'name': dlProfileData.name
                    }
                    , 'id': newTerminKey
                    , 'start': t.start
                    , 'end': t.end
                };
                userTermineRef.child(newTerminKey).set(newT);
                var newTerminRef = firebasedb.ref('termine/'+userID+'/'+newTerminKey).child("id");
                newTerminRef.set(newTerminKey);
                var dlTermineRef = firebasedb.ref('termine/' + t.dienstleister).child(newTerminKey);
                var terminLeistungRef = firebasedb.ref('termineLeistungen/' + newTerminKey).child('Leistungen');
                var newT = {
                    'dln': {
                        'id': userID
                        , 'name': user.providerDataInternal[0].displayNameInternal
                    }
                    , 'id': newTerminKey
                    , 'start': t.start
                    , 'end': t.end
                };
                dlTermineRef.set(newT);
                var tleistung = t.leistungen
                var tmpObj;
                
                for (e in tleistung) {
                    terminLeistungRef.child(e).set(leistungenByDl[tleistung[e]]);
                }
                
                
                firebasedb.ref('customers').child(t.dienstleister).child(userID).set({
                    "id": userID
                    , "name": user.providerDataInternal[0].displayNameInternal
                });
                cb(null,true);
            }).catch(function(err){
                console.log(err);
                cb(err);
            });
        }).catch(function(err){
                console.log(err);
                cb(err);
            });
    })
}

/**
 * Checks if dl is matching the services specified in appointment t
 * @param {termin} t 
 * @param {function} cb
 * @return {bool} true
 * @throws Service not found.
 */
function checkServices(t, cb) {
    var dlLeistungRef = firebasedb.ref('leistungen').child(t.dienstleister);
    dlLeistungRef.once('value').then(function (snapshot) {
        var dlLeistungen = snapshot.val();
        var leistungen = t.leistungen;
        var i = 0;
        var j = 0;
        for (item in leistungen) {
            j++;
            for (item2 in dlLeistungen) {
                if (leistungen[item] == item2) {
                    i++;
                }
            }
        }
        if (i != j) {
            throw ('Dienleistung nicht gefunden.');
        };
    }).then(function (t) {
        cb(null, true);
    }, function (err) {
        cb(err);
    });
}

/**
 * Checks checks if the there are appointments blocking the appointment t of user
 * @param {termin} t 
 * @param {string} id
 * @param {function} cb
 * @return {bool} true
 * @throws Termin liegt in Termin: {Termin}
 */
function checkTermine(t, id, cb) {
    var dlTermineRef = firebasedb.ref('termine').child(id);
    dlTermineRef.once('value').then(function (snapshot) {
        var appointmentsFromID = snapshot.val();
        for (item in appointmentsFromID) {
            if ((t.start >= appointmentsFromID[item].start) && (t.end <= appointmentsFromID[item].end)) {
                throw ('Termin liegt in Termin: ' + appointmentsFromID[item].start); //liegt in Abwesenheit
            }
        }
    }).then(function (t) {
        cb(null, true);
    }, function (err) {
        cb(err);
    });
}

/**
 * Checks the away times of the dl
 * @param {termin} t 
 * @param {function} cb
 * @return {bool} true
 * @throws Termin liegt in Abwesenheit: {Termin}
 */
function checkAbwesenheit(t, cb) {
    var myDuration = moment.duration(moment.unix(t.end).diff(moment.unix(t.start))).asHours();
    var dlAbwesenRef = firebasedb.ref('abwesenheiten').child(t.dienstleister);
    dlAbwesenRef.once('value').then(function (snapshot) {
        var abwesenheitsTermine = snapshot.val();
        for (var item in abwesenheitsTermine) {
            if ((t.start >= abwesenheitsTermine[item].start) && (t.end <= abwesenheitsTermine[item].end)) {
                throw ('Termin liegt in Abwesenheit: ' + abwesenheitsTermine[item].titel); //liegt in Abwesenheit
            }
        }
    }).then(function (t) {
        cb(null, true);
    }, function (err) {
        cb(err);
    });
}


/**
 * Checks the opening times of the dl
 * @param {termin} t 
 * @param {function} cb
 * @return {bool} true
 * @throws Termin liegt nicht in Öffnungszeit: {Termin}
 */
function checkÖffnung(t, cb) {
    var openDayArray = daysBetweenTwoMoments(moment.unix(t.start), moment.unix(t.end));
    var dlOeffnungsRef = firebasedb.ref('oeffnungszeiten/' + t.dienstleister);
    dlOeffnungsRef.once('value').then(function (snapshot) {
        var tmpSnapshot = snapshot.val();
        for (i in openDayArray) {
            var tmpMoment = moment.unix(openDayArray[i]);
            var tmpTerminEnd = moment.unix(openDayArray[i]).hours(moment.unix(t.end).hours()).minutes(moment.unix(t.end).minutes()).seconds(moment.unix(t.end).seconds());
            var tmpMomentWeekDay = moment.unix(openDayArray[i]).weekday();
            var tmpMomentStartAm = moment(tmpSnapshot[tmpMomentWeekDay].startAm, "hh:mm").day(tmpMoment.day()).month(tmpMoment.month()).year(tmpMoment.year());
            var tmpMomentEndPm = moment(tmpSnapshot[tmpMomentWeekDay].endPm, "hh:mm").day(tmpMoment.day()).month(tmpMoment.month()).year(tmpMoment.year());
            var tmpMomentEndAm = moment(tmpSnapshot[tmpMomentWeekDay].endAm, "hh:mm").day(tmpMoment.day()).month(tmpMoment.month()).year(tmpMoment.year());
            var tmpMomentStartPm = moment(tmpSnapshot[tmpMomentWeekDay].startPm, "hh:mm").day(tmpMoment.day()).month(tmpMoment.month()).year(tmpMoment.year());
            //Tag hat keine Mittagspause  
            if (!tmpMoment.isBetween(tmpMomentStartAm, tmpMomentEndPm) && !tmpTerminEnd.isBetween(tmpMomentStartAm, tmpMomentEndPm)) {
                throw ('Termin liegt nicht in Öffnungszeit: ' + JSON.stringify({
                    "start": tmpMoment
                    , "end": tmpTerminEnd
                }));
                break;
            }
            if (tmpMomentEndAm.isValid() && tmpMomentStartPm.isValid()) {
                if (!(tmpMoment.isBetween(tmpMomentStartAm, tmpMomentEndAm, null, '[]') && tmpTerminEnd.isBetween(tmpMomentStartAm, tmpMomentEndAm, null, '[]') || tmpMoment.isBetween(tmpMomentStartPm, tmpMomentEndPm, null, '[]') && tmpTerminEnd.isBetween(tmpMomentStartPm, tmpMomentEndPm, null, '[]'))) {
                    console.log(tmpTerminEnd.hours());
                    throw ('Termin liegt nicht in Öffnungszeit: ' + JSON.stringify({
                        "start": tmpMoment
                        , "end": tmpTerminEnd
                    }));
                    break;
                }
            }
        }
        cb(null, true);
    }).then(function (t) {
        cb(null, true);
    }, function (err) {
        cb(err);
    });
}

/**
 * Gets all days between two moment objs with time = a.time
 * @param {moment} a 
 * @param {moment} b
 * @return {array} Unix Timestamps
 */
function daysBetweenTwoMoments(a, b) {
    var daysBetween = [];
    var diff = b.diff(a, 'days');
    var curentDay = a;
    if (diff == NaN || diff == 0) {
        var diff = 0;
    }
    for (i = 0; i <= diff; i++) {
        daysBetween.push(curentDay.unix());
        curentDay = curentDay.add(24, 'hours');
    }
    return daysBetween;
}