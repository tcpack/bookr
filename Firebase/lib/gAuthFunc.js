var fs = require('fs');
var googleAuth = require('google-auth-library');

var exports = module.exports = {
    gAuth: function (token, userID, cb) {
        fs.readFile('../Firebase/config/client_secret.json', function processClientSecrets(err, content) {
            if (err) {
                cb('Error loading client secret file: ' + err);
            }
            else {
                authorize(JSON.parse(content), token, userID, function (err, res) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        cb(null, res);
                    }
                });
            }
        });
    }
};

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, token, u, cb) {
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    var redirectUrl = credentials.web.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
    var dlUserRef = firebase.database().ref('user/' + u + '/gAuth');
    var t = token;
    if (t == '') {
        dlUserRef.once('value', function (snapshot) {
            var dbToken = snapshot.val();
            oauth2Client.credentials = dbToken;
            if (dbToken == null) {
                cb("Kein Zugriff auf die Google API");
            } else {
                cb(null, oauth2Client);    
            }
        }, function (err) {
            cb(err);
        });
    }
    else {
        getNewToken(oauth2Client,t, u, function (err, res) {
            if (err) {
                cb(err);
            }
            else {
                oauth2Client.credentials = t;
                cb(null, oauth2Client);
            }
        });
    }
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, mytoken, u, cb) {
    oauth2Client.getToken(mytoken, function (err, token) {
        if (err) {
            cb('Error while trying to retrieve access token ' + err);
        }
        else {
            oauth2Client.credentials = token;
            storeToken(token, u, function (err, res) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null, oauth2Client);
                }
            });
        }
    });
}
/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token, userID, cb) {
    var dlUserRef = firebase.database().ref('user/' + userID).child('gAuth');
    try {
        dlUserRef.set(token);
        cb(null, true)
    }
    catch (err) {
        cb(err);
    }
}